export default {
	label: 'Country',
	name: 'country',
	widget: 'string',
	hint: `In which country is this city located, ex. Germany`,
	required: true,
}
