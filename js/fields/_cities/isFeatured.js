export default {
	label: 'Is Featured',
	name: 'is_featured',
	widget: 'boolean',
	defaut: 'false',
	hint: `Is this city featured among all other cities`,
	required: false,
}
