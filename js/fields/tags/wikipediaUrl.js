export default {
	label: 'Wikipedia URL',
	name: 'wikipedia_url',
	widget: 'string',
	hint: `Is there a Wikipedia page for this tag?`,
	required: false,
}
