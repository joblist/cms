export default {
	label: 'Is Featured',
	name: 'is_featured',
	widget: 'boolean',
	defaut: 'false',
	hint: `Is this tag featured among all other tags`,
	required: false,
}
