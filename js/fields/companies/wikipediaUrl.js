export default {
	label: 'Wikipedia URL',
	name: 'wikipedia_url',
	widget: 'string',
	hint: `The Wikipedia URL address pointing to this company profile`,
	required: false
}
