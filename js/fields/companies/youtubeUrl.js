export default {
	label: 'Youtube URL',
	name: 'youtube_url',
	widget: 'string',
	hint: `The Youtube URL address pointing to this company profile`,
	required: false
}
