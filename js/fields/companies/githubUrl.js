export default {
	label: 'Github URL',
	name: 'github_url',
	widget: 'string',
	hint: `The Github URL address pointing to this company profile`,
	required: false
}
