export default {
	label: "Job board URL",
	name: "job_board_url",
	widget: "string",
	hint: "The URL to the company's career's page, ex: https://example.com/careers, or to its job board profile; ex: https://my-company.greenhouse.com",
	required: true,
};
